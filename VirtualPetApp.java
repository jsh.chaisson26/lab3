import java.util.Scanner;
public class VirtualPetApp
{
	public static void main(String[] args)
	{
		Scanner reader = new Scanner(System.in);
		Lion[] pride = new Lion[4];
		//Making an array of objects using the class file made.
		for(int i = 0; i < pride.length; i++)
		//Does a loop that makes an animal object at each index.
		//After that it asks the user questions about the fields and take their user input.
		{
			pride[i]= new Lion();
			System.out.println("How much does your animal weigh in pounds?");
			pride[i].weight = Integer.parseInt(reader.nextLine());
			System.out.println("How many teeth does your animal have?");
			pride[i].numOfTeeth = Integer.parseInt(reader.nextLine());
			System.out.println("Whic family does your animal belong to?");
			pride[i].family = reader.nextLine();
		}
		//Prints the answers of the questions asked for the last object in the array from the for loop.
		System.out.println(pride[pride.length-1].weight);
		System.out.println(pride[pride.length-1].numOfTeeth);
		System.out.println(pride[pride.length-1].family);
		System.out.print("\n");
		//Calls the 2 instance methods for the first object in the array.
		pride[0].isObese();
		pride[0].biteDamage();
	}
}